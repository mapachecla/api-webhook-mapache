const express = require('express')
const dotenv = require('dotenv')
const fetch = require('node-fetch')
const nodemailer = require('nodemailer')
const mailgun = require('mailgun-js')
const mg = mailgun({ apiKey: process.env.MAILGUN_APIKEY, domain: process.env.DOMAIN_MAILGUN })

const { google } = require('googleapis')
const OAuth2 = google.auth.OAuth2

const app = express()

dotenv.config()

// --- Envía email de aviso de compra al vendedor y email con link de descarga al comprador --- //
const webhookMercadopago = (req, res) => {

    var data = req.body
    res.sendStatus(200)

    console.log(`** El ID de pago es: ${data.data.id} **`)

    var id_venta = data.data.id
    const token = process.env.ACCESS_TOKEN

    var url_download
    var song
    var email

    async function obtenerDatos() {

        let url = `https://api.mercadopago.com/v1/payments/${id_venta}?access_token=${token}`;
        let response = await fetch(url);
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }
        let myJson = await response.json();
        let email = myJson.payer.email
        let song = myJson.description

        return [email, song]
    }

    obtenerDatos()
        .then((res) => {
            email = res[0]
            song = res[1]
            console.log(`la cancion es: ${song}`)
            console.log(`email es: ${email}`)

            if (song === 'te_lo_prometo') {
                url_download = process.env.SONG_TELOPROMETO
            } else if (song === 'tres') {
                url_download = process.env.SONG_TRES
            } else if (song === 'bosques_mares') {
                url_download = process.env.SONG_BOSQUES
            } else if (song === 'caricias') {
                url_download = process.env.SONG_CARICIAS
            } else if (song === 'en_segunda') {
                url_download = process.env.SONG_ENSEGUNDA
            } else if (song === '731') {
                url_download = process.env.SONG_731
            }

            // --- Envío de link de descargar --- //
            const envioGoogle = async() => {

                const outputHTML = `
                                <h2>Gracias por comprar mi canción!</h2>
                                <h3>Por favor hace click en el siguiente enlace o en el botón para descargarla!</h3>
                                <p>${url_download}<p>
                                <a href="${url_download}"><button>Descargá el disco!</a>
                            `;

                const oauth2Client = new OAuth2(
                    process.env.CLIENT_ID,
                    process.env.CLIENTE_SECRET,
                    process.env.REDIRECT_URL
                )

                oauth2Client.setCredentials({
                    refres_token: process.env.REFRESH_TOKEN
                })

                const accessToken = oauth2Client.getAccessToken()


                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        type: "OAuth2",
                        user: "nodejsa@gmail.com",
                        clientId: process.env.CLIENT_ID,
                        clientSecret: process.env.CLIENTE_SECRET,
                        refreshToken: process.env.REFRESH_TOKEN,
                        accessToken: accessToken
                    }
                })

                const mailOptions = {
                    from: 'Claudio Herrera <no-reply@escuadron.com>',
                    to: 'claudiophv@gmail.com',
                    subject: 'Ya podés descargar la canción!!',
                    generateTextFromHTML: true,
                    html: outputHTML
                }

                transporter.sendMail(mailOptions, (err, info) => {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log('Email enviado exitosamente: %s', info.messageId)
                    }
                })

            }
            envioGoogle()

        }).catch(e => console.log(e))


    // --- Envío de aviso de pago --- //
    const envioMailgun = async() => {
        const contentHTML = `
            <h1>Información de la venta</h1>
            <ul>
                <li>ID de pago: ${data.data.id}</li>
                <li>ID de usuario: ${data.user_id}</li>
                <li>Fecha de venta: ${data.date_created}</li>
            </ul>
        `;

        const mail = {
            from: 'no.reply@escuadron.com',
            to: 'escuadron@protonmail.com',
            subject: 'Se ha realizado una venta ✔',
            generateTextFromHTML: true,
            html: contentHTML
        }

        mg.messages().send(mail, function(err, body) {
            if (err) {
                console.log(`Error al enviar correo: ${err.message}`)
            } else {
                console.log(body)
            }
        })
    }


    envioMailgun()
}



// --- METODO PARA HACER PRUEBAS POR GET PARA ENVÍO DE LINK DE DESCARGA --- //
// const getPaymentTest = (req, res) => {

//     var id_venta = 12728142702 // acá usaré data.id para sacar el ID de la operación
//     const token = process.env.ACCESS_TOKEN

//     var url = `https://api.mercadopago.com/v1/payments/${id_venta}?access_token=${token}`

//     fetch(url)
//         .then(res => res.json())
//         .then(json => {

//             var email = json.payer.email
//             console.log(`Email del comprador: ${email}`),
//                 res.send({ Email: email })

//             // -- Envío de link de descarga -- //
//             const url_download = process.env.DOWNLOAD_URL
//             const contentHTML = `
//                 <h2>Gracias por comprar nuestro Disco!!</h2>
//                 <h3>Por favor hace click en el siguiente enlace o en el botón para descargarlo!</h3>
//                 <p>${url_download}<p>
//                 <a href="${url_download}"><button>Descargá el disco!</a>
//             `;

//             const oauth2Client = new OAuth2(
//                 process.env.CLIENT_ID,
//                 process.env.CLIENTE_SECRET,
//                 process.env.REDIRECT_URL
//             )

//             oauth2Client.setCredentials({
//                 refres_token: process.env.REFRESH_TOKEN
//             })

//             const accessToken = oauth2Client.getAccessToken()

//             const transporter = nodemailer.createTransport({
//                 service: 'gmail',
//                 auth: {
//                     type: "OAuth2",
//                     user: "nodejsa@gmail.com",
//                     clientId: process.env.CLIENT_ID,
//                     clientSecret: process.env.CLIENTE_SECRET,
//                     refreshToken: process.env.REFRESH_TOKEN,
//                     accessToken: accessToken
//                 }
//             })

//             const mailOptions = {
//                 from: 'ESCUADRÓN <no-reply@escuadron.com>',
//                 to: 'claudiophv@gmail.com',
//                 subject: 'Compraste Volumen 1, descargalo!!',
//                 generateTextFromHTML: true,
//                 html: contentHTML
//             }

//             transporter.sendMail(mailOptions, (err, info) => {
//                 if (err) {
//                     console.log(err)
//                 } else {
//                     console.log('Email enviado exitosamente: %s', info.messageId)
//                 }
//             })

//         })
//         .catch(err => {
//             console.error(err)
//         })

// }


module.exports = {
    webhookMercadopago,
    //getPaymentTest
}